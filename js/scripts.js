(function () {
  const main = document.querySelector(".main");
  const sliderItems = document.querySelectorAll(".main-slider__item");

  sliderItems.forEach(
    (item) => (item.style.height = main.clientHeight + +10 + "px")
  );
})();
